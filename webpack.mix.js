const mix = require('laravel-mix');
const glob = require('glob');
const { CleanWebpackPlugin } = require('clean-webpack-plugin'); // installed via npm
const fs = require('fs');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

const packagePath = 'anon/views'; // laravel view 경로
const dir = 'public/assets'; // js/css 가 build 될 장소

mix.setPublicPath(dir);
mix.setResourceRoot('/assets'); // scss 에서 이미지/웹폰트 url prefix
mix.options({
    extractVueStyles: false,
    processCssUrls: true,
    terser: {},
    purifyCss: false,
    //purifyCss: {},
    postCss: [require('autoprefixer')],
    clearConsole: false,
    cssNano: {
        // discardComments: {removeAll: true},
    }
});

mix.webpackConfig({
    resolve: {
        alias: {
        }
    },
    plugins: [
        new CleanWebpackPlugin()
    ]
    // , devtool: 'source-map'
});

mix.extract();

const tmpRegex = new RegExp(`^(\.\/)?${packagePath}\/`, 'g');
let emptyFile;


//------------------------------
// ja compile
//------------------------------
const js = glob.sync(`./${packagePath}/**/_assets/*.js`);
js.forEach(function(file) {
    if (/_assets\/empty\.js$/.test(file)) {
        emptyFile = file;
        return;
    }
    let output = file;
    output = output.replace(tmpRegex, '');
    output = output.replace(/^(.+)?_assets\/(.+)\.js$/g, '$1$2.js');
    mix.js(file, output);
});


//------------------------------
// scss compile
//------------------------------
const scss = glob.sync(`./${packagePath}/**/_assets/*.scss`);
scss.forEach(function(file) {
    let output = file;
    output = output.replace(tmpRegex, '');
    output = output.replace(/^(.+)?_assets\/(.+)\.scss$/g, '$1$2.css');
    mix.sass(file, output);
});

if (emptyFile) {
    mix.js(emptyFile, 'empty.js');
}

if (mix.inProduction()) {
    mix.version();
} else {
}
