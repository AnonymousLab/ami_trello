순서
```
composer isntall
php artisan migrate
npm install
npm run prod
```

crontab
```
* * * * * apache php /data/www/ami_trello/artisan schedule:run >> /dev/null 2>&1
```

로컬 컴퓨터에서 npm run prod 로 js/css 빌드 한다음 바로 배포한다.  
publish/assets 은 git 에 포함되어야 함.  
서버에서 npm run 할 필요가 없음.  

서버는
```
git pull
```
```
php artisan migrate
```
```
php artisan optimize
```
이런 것만 하면 됨.
