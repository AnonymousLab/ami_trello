<?php


namespace Anon\Classes;

use Anon\Classes\Config;
use Anon\Models\CardModel;
use Anon\Models\ListModel;
use Anon\Models\MemberModel;
use Illuminate\Database\Eloquent\Builder;

class Util extends AnonInstance
{
    private $privateCache = [];


    function checkConfigRequired()
    {
        $config = Config::gi();

        $checkConfig = [
            'trello_board_id',
            'trello_list',
            'trello_used_list',
            'trello_todo_list_id',
            'trello_doing_list_id',
            'trello_done_list_id',
        ];

        $err = [];
        foreach ($checkConfig AS $configKey) {
            if (empty($config->get($configKey))) {
                $err[] = $configKey;
            }
        }

        // member 추출
        $members = Util::gi()->getMembers();
        if ($members->isEmpty()) {
            $err[] = '사용자 동기화';
        }

        return $err;
    }

    /**
     * 리스트 이름 추출
     *
     * @param  null  $id
     * @return array|mixed|string
     */
    function getListName($id = null)
    {
        $cacheKey = 'get_list_name_' . ($id ?: '');
        if (!empty($this->privateCache[$cacheKey])) {
            return $this->privateCache[$cacheKey];
        }

        $list = Config::gi()->get('trello_list');
        if (empty($list)) {
            return '';
        }

        $list2 = [];
        foreach ($list AS $li) {
            $list2[$li->id] = $li->name;
        }

        if (empty($id)) {
            $rt = $list2;
        } else {
            $rt = $list2[$id] ?? '';
        }

        $this->privateCache[$cacheKey] = $rt;
        return $rt;
    }


    /**
     * DB 에서 list id 추출
     *
     * @return ListModel[]|\Illuminate\Database\Eloquent\Collection
     */
    function getLists()
    {
        // return ListModel::all()->keyBy('id');
        $list = Config::gi()->get('trello_list', null);
        $list2 = collect($list);

        return $list2->keyBy('id');
    }


    /**
     * to do, doing, done list id 추출
     * @return array
     */
    function getTodoDoingDoneList()
    {
        $config = Config::gi();

        $list = [];

        $todoListId = $config->get('trello_todo_list_id');
        $listName = $this->getListName($todoListId);
        $list['todo'] = ['name' => $listName, 'id' => $todoListId];

        $doingListId = $config->get('trello_doing_list_id');
        $listName = $this->getListName($doingListId);
        $list['doing'] = ['name' => $listName, 'id' => $doingListId];

        $doneListId = $config->get('trello_done_list_id');
        $listName = $this->getListName($doneListId);
        $list['done'] = ['name' => $listName, 'id' => $doneListId];

        return $list;
    }


    /**
     * setting 의 used list 추출
     *
     * @return array
     */
    function getUsedListIds()
    {
        $config = Config::gi();

        $listIds = [];

        // to do, doing, done list id 를 배열 앞으로
        $tmp = $this->getTodoDoingDoneList() ?: [];
        foreach ($tmp AS $v) {
            $name = $v['name'];
            $listIds[$name] = $v['id'];
        }

        $usedList = $config->get('trello_used_list', []);
        foreach ($usedList AS $k => $v) {
            if (in_array($v, $listIds)) {
                continue;
            }
            $listName = $this->getListName($v);
            $listIds[$listName] = $v;
        }
        // $listIds = array_filter($listIds);

        return $listIds;
    }


    /**
     * 회원 추출
     *
     * @return mixed
     */
    function getMembers()
    {
        return MemberModel::where('username', '<>', 'mckim16')->orderby('full_name')->get();
    }


    /**
     * 넘어온 데이터를 gantt 형 json 으로 변환
     *
     * @param $args
     * @return array[]
     */
    function ganttJson($args)
    {
        $cards = $args['cards'];
        $members = $args['members'];
        $memberArr = $members->keyBy('id')->toArray();

        $i = 1;
        $odd = 0;
        $output = [];
        foreach ($cards AS $mbId => $card1) {
            if ($memberArr[$mbId]['full_name'] === 'mckim') {
                continue;
            }
            $odd++;

            $parentId = $i++;
            $output[] = [
                'id' => $parentId,
                'text' => $memberArr[$mbId]['full_name'],
                'start_date' => null,
                // 'end_date' => null,
                'open' => true,
                'duration' => false,

                'is_header' => true,
                'owner' => $mbId,
            ];


            foreach ($card1 as $key => $card) {
                $listId = $i++;
                $output[] = [
                    'id' => $listId,
                    'text' => ucfirst($key),
                    'start_date' => null,
                    'end_date' => null,
                    'open' => true,
                    'duration' => null,
                    'parent' => $parentId,

                    'odd' => $odd%2,
                    'is_list' => true,
                    'owner' => $mbId,
                ];

                foreach ($card as $c) {
                    $startDate = null;
                    if (!empty($c['time_start'])) {
                        $startDate = date('Y-m-d 00:00', strtotime($c['time_start']));
                    } elseif (!empty($c['time_create'])) {
                        $startDate = date('Y-m-d 00:00', strtotime($c['time_create']));
                    }


                    $endDate = date('Y-m-d 23:59');
                    if (!empty($c['due']) && $c['due_complete'] === 1) {
                        $endDate = date('Y-m-d 23:59', strtotime($c['due']));
                    } elseif (!empty($c['time_done'])) {
                        $endDate = date('Y-m-d 23:59', strtotime($c['time_done']));
                    } elseif (!empty($c['due'])) {
                        $endDate = date('Y-m-d 23:59', strtotime($c['due']));
                    }

                    if ($endDate < $startDate) {
                        $endDate = null;
                    }

                    $tmp = [
                        'id' => $i++,
                        'text' => $c['name'],
                        'desc' => $c['desc'],
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'duration' => null,
                        'parent' => $listId,

                        'odd' => $odd%2,
                        'owner' => $mbId,
                    ];
                    $output[] = $tmp;
                }
            }
        }

        return ['data' => $output];
    }


    /**
     * DB에서 card list 추출
     *
     * @param  array  $args
     * @return mixed
     */
    function getCardList($args = [])
    {
        $filterMemberId = $args['member_id'] ?? null;
        $filterTimeDone = $args['time_done'] ?? null;
        $filterType = $args['type'] ?? null;
        $page = $args['page'] ?? 1;
        $limit = $args['limit'] ?? 20;
        $isHidden = $args['is_hidden'] ?? null;

        $query = new CardModel;

        if ($filterType === 'complete-card') {
            $query = $query->whereNotNull('time_done');
        } elseif(!empty($filterType)) {
            $query = $query->where('id_list', $filterType);
        }

        if ($isHidden !== null) {
            $query = $query->where('is_hidden', $isHidden);
        }

        // if (!empty($filterTimeDone)) {
        //     $query = $query->where();
        // }

        if (!empty($filterMemberId)) {
            $query = $query->whereHas('cardListWithMember', function (Builder $query) use ($filterMemberId) {
                $query->where('member_id', $filterMemberId);
            });
        }

        if ($filterType === 'complete-card') {
            $query = $query->orderby('time_done', 'desc');
        } else {
            $query = $query->orderby('time_create', 'desc');
        }

        return $query->paginate($limit, ['*'], 'page', $page);
    }
}
