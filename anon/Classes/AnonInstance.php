<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2017. 10. 30.
 * Time: PM 12:52
 */

namespace Anon\Classes;


class AnonInstance
{
    /**
     * @return $this
     */
    final public static function gi()
    {
        static $instances = array();

        $calledClass = get_called_class();

        if (!isset($instances[$calledClass]))
        {
            $instances[$calledClass] = new $calledClass();
        }

        return $instances[$calledClass];
    }

    final private function __clone() { }

    //--------------------------------------------------------------------------

    /**
     * 임시용
     *
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    static function __callStatic($method, $args)
    {
        $instance = static::gi();

        $method = substr($method, 1);

        switch (count($args))
        {
            case 0:
                return $instance->$method();

            case 1:
                return $instance->$method($args[0]);

            case 2:
                return $instance->$method($args[0], $args[1]);

            case 3:
                return $instance->$method($args[0], $args[1], $args[2]);

            case 4:
                return $instance->$method($args[0], $args[1], $args[2], $args[3]);

            default:
                return call_user_func_array(array($instance, $method), $args);
        }
    }
}