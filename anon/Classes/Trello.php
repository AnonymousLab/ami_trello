<?php


namespace Anon\Classes;

use Anon\Models\CardModel;
use Anon\Models\MemberCardModel;
use Anon\Models\MemberModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Trello extends AnonInstance
{
    private $privateCache = [];

    function getBoardId()
    {
        return Config::gi()->get('trello_board_id');
    }


    /**
     * 트렐로 시간을 포멧을 변경
     * @param $trelloDate
     * @return string
     */
    function changeDate($trelloDate)
    {
        try {
            $dt = new \DateTime($trelloDate);
            $dt->setTimezone(new \DateTimeZone('Asia/Seoul'));
            $rt = $dt->format('Y-m-d H:i:s');
        } catch (\Throwable $e) {
            $rt = null;
        }

        return $rt;
    }


    /**
     * 멤버 동기화
     *
     * @return mixed
     */
    function memberSync()
    {
        $boardId = $this->getBoardId();

        if (empty($boardId)) {
            return [false, null, 'board id 없음'];
        }

        [$status, $members, $msg] = TrelloApi::gi()->getMemberList($boardId);

        if (!$status) {
            return $msg;
        }

        foreach($members AS $member) {
            $tmp = snakeKeys($member);
            MemberModel::updateOrCreate(
                ['id' => $member['id']],
                $tmp
            );
        }
    }



    /**
     * list 동기화
     *
     * @return array
     */
    function syncList()
    {
        $boardId = $this->getBoardId();
        if (empty($boardId)) {
            return [false, null, 'board id 없음'];
        }
        [$status, $data, $message] = $rt = TrelloApi::gi()->getLists($boardId);

        if (!$status) {
            return $rt;
        }

        Config::gi()->set('trello_list', $data);

        return [true, $data, null];
    }



    /**
     * 카드 하나를 동기화 한다.
     * @param $id
     * @return array
     */
    function syncOneCard($id)
    {
        [$status, $info, $msg] = $rt = TrelloApi::gi()->getCardInfo($id);

        if ($status){
            $info2 = snakeKeys($info);
            $info2['due'] = $this->changeDate($info['due']);
            $info2 = $this->makeCardTimeInfo($info2);
            CardModel::updateOrCreate(
                ['id' => $info2['id']],
                $info2
            );
        }

        return $rt;
    }



    /**
     * List 의 Card 동기화
     *
     * @param  null  $type
     * @return array|mixed|string|null
     */
    function syncCards($type = null)
    {
        $toDoListListId = Config::gi()->get('trello_todo_list_id');
        $doingListId = Config::gi()->get('trello_doing_list_id');
        $doneListId = Config::gi()->get('trello_done_list_id');

        if (empty($toDoListListId)) {
            return 'To Do list ID 값이 설정되지 않음.';
        } elseif (empty($doingListId)) {
            return 'Doing list ID 값이 설정되지 않음.';
        } elseif (empty($doneListId)) {
            return 'Done list ID 값이 설정되지 않음.';
        }

        $listType = [
            'todo' => $toDoListListId,
            'doing' => $doingListId,
            'done' => $doneListId,
        ];

        // type 값이 없으면 모든 used list 를 동기화 한다.
        if (empty($type)) {
            $usedList = Config::gi()->get('trello_used_list', []);
            foreach ($usedList AS $li) {
                $this->syncCards($li);
            }
            return null;
        }

        // $type 값이 to do, doing, done 이면 $listType 변수에서 list id를 가져온다.
        $listId = $listType[$type] ?? $type;
        if (empty($listId)) {
            return "{$type} is not found";
        }

        $this->syncCardsExec($listId);
    }


    /**
     * List 의 Card 동기화 실행
     * $listId 값이 sync_archive_cards 라면 archive 된 card 들을 동기화 한다.
     *
     * @param $listId
     * @return mixed
     */
    function syncCardsExec($listId)
    {
        $toDoListListId = Config::gi()->get('trello_todo_list_id');
        $doingListId = Config::gi()->get('trello_doing_list_id');
        $doneListId = Config::gi()->get('trello_done_list_id');

        if ($listId === 'sync_archive_cards') {
            $boardId = $this->getBoardId();
            [$status, $cards, $msg] =  TrelloApi::gi()->getArchiveCards($boardId);
        } else {
            [$status, $cards, $msg] =  TrelloApi::gi()->getCardsInList($listId);
        }

        if (!$status) {
            return $msg;
        }

        CardModel::where('id_list', $listId)->update([
            'id_list' => '',
        ]);

        foreach ($cards AS $card) {
            // 디비에 입력하기 위해서 camel 형태의 키를 snake 로 변경한다.
            $tmp = snakeKeys($card);
            $tmp = $this->makeCardTimeInfo($tmp);

            CardModel::updateOrCreate(
                ['id' => $tmp['id']],
                $tmp
            );

            if (!empty($tmp['id_members'])) {
                MemberCardModel::where('card_id', $tmp['id'])->delete();
                foreach ($tmp['id_members'] AS $mb) {
                    MemberCardModel::firstOrCreate(
                        [
                            'card_id' => $tmp['id'],
                            'member_id' => $mb
                        ]
                    );
                }
            }
            usleep(500000);
        }

        $this->syncCardIdListField();
    }


    /**
     * 디비에 입력 할 time 정보들을 생성한다.
     *
     * @param $card
     * @return array
     */
    function makeCardTimeInfo($card)
    {
        $tmp = $card;

        $toDoListListId = Config::gi()->get('trello_todo_list_id');
        $doingListId = Config::gi()->get('trello_doing_list_id');
        $doneListId = Config::gi()->get('trello_done_list_id');

        $tmp['time_create'] = TrelloApi::gi()->getCreateTime($card['id']);
        $tmp['time_start'] = null;
        $tmp['time_done'] = null;

        if (!empty($tmp['due'])) {
            try {
                $dt = new \DateTime($tmp['due']);
                $dt->setTimezone(new \DateTimeZone('Asia/Seoul'));
                $tmp['due'] = $dt->format('Y-m-d H:i:s');
            } catch (\Throwable $e) {}
        }

        $cardIdList = $tmp['id_list'];

        if ($cardIdList === $doingListId) {
            $tmp['time_start'] = TrelloApi::gi()->getCardMovedTime($card['id'], $doingListId);

        } elseif ($cardIdList === $doneListId) {
            $tmp['time_start'] = TrelloApi::gi()->getCardMovedTime($card['id'], $doingListId);
            $tmp['time_done'] = TrelloApi::gi()->getCardMovedTime($card['id'], $doneListId);

            if (empty($tmp['time_done'])) {
                $tmp['time_done'] = TrelloApi::gi()->getCreateTime($card['id']);
            }
        }

        if (($cardIdList === $doingListId || $cardIdList === $doneListId) && empty($tmp['time_start'])) {
            $tmp['time_start'] = TrelloApi::gi()->getCreateTime($card['id']);
        }

        return $tmp;
    }



    /**
     * ld_list 컬럼이 비어 있는 card 를 찾아 ld_list 값을 동기화 시킨다.
     */
    function syncCardIdListField()
    {
        $cards = CardModel::where('id_list', '')->get();

        foreach ($cards AS $card) {
            [$status, $info, $msg] = TrelloApi::gi()->getCardInfo($card['id']);
            if (!$status) {
                CardModel::where('id', $card['id'])->delete();
                continue;
            }

            $card->id_list = $info['idList'];
            $card->save();
            usleep(500000);
        }
    }


    function deleteCard($cardId)
    {
        [$status, $data, $msg] = $rt = TrelloApi::gi()->deleteCard($cardId);
        if ($status) {
            CardModel::where('id', $cardId)->delete();
        }

        return $rt;
    }


    /**
     * 카드 완료 처리
     * @param $cardId
     * @return array
     */
    function completeCard($cardId)
    {
        $listId = Config::gi()->get('trello_done_list_id');

        if (empty($listId)) {
            return [false, null, 'Done list id 를 설정해주세요'];
        }

        $data = [
            // 'due' => date('Y-m-d H:i:s'),
            'due' => Carbon::now()->toISOString(),
            'dueComplete' => true,
            'idList' => $listId,
        ];

        [$status, $data, $msg] = $rt = TrelloApi::gi()->updateCard($cardId, $data);
        $data2 = snakeKeys($data);
        $data2['due'] = $this->changeDate($data2['due']);
        $data2['time_done'] = date('Y-m-d H:i:s');
        CardModel::updateOrCreate(
            ['id' => $data2['id']],
            $data2
        );

        return $rt;
    }
}
