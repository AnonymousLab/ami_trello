<?php


namespace Anon\Classes;

use Illuminate\Support\Facades\Cache as BaseCache;
use Closure;
use Illuminate\Support\Carbon;

class ACache extends AnonInstance
{
    protected $cache;
    private $cacheDriver = 'file';

    function __construct()
    {
        $this->setDriver($this->cacheDriver);
    }

    function setDriver($driver)
    {
        $this->cache = BaseCache::store($driver);
    }

    // --------------------------------------------------------------------------

    /**
     * 캐시 추가
     * @param        $key
     * @param        $val
     * @param int    $expireTime 초
     */
    function put($key, $val, $expireTime = 60)
    {
        $expire = $this->convertTime($expireTime);
        $this->cache->put($key, $val, $expire);
    }

    /**
     * alias
     * @param $key
     * @param $val
     * @param int $expireTime
     */
    function set($key, $val, $expireTime = 60)
    {
        $this->put($key, $val, $expireTime);
    }

    //--------------------------------------------------------------------------

    function remember($key, $expireTime, Closure $func)
    {
        $expire = $this->convertTime($expireTime);
        return $this->cache->remember($key, $expire, $func);
    }

    //--------------------------------------------------------------------------

    /**
     * cache get
     * @param $key
     * @param null $default
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function get($key, $default = null)
    {
        return $this->cache->get($key, $default);
    }

    /**
     * 캐시 가져오고 삭제
     * @param $key
     * @param null $default
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function pull($key, $default = null)
    {
        $rt = $this->cache->get($key, $default);
        $this->cache->forget($key);

        return $rt;
    }

    //--------------------------------------------------------------------------

    /**
     * cache 삭제
     * @param $key
     */
    function forget($key)
    {
        $this->cache->forget($key);
    }

    /**
     * alias
     * @param $key
     */
    function delete($key)
    {
        $this->forget($key);
    }

    //--------------------------------------------------------------------------

    /**
     * 캐시 전체 비우기
     */
    function flush()
    {
        $this->cache->flush();
    }


    /**
     * 캐시 exists
     * @param $key
     * @return mixed
     */
    function has($key)
    {
        return $this->cache->has($key);
    }

    //--------------------------------------------------------------------------

    function convertTime($expireTime)
    {
        if (is_string($expireTime)) {
            $expireTime = trim($expireTime);
            preg_match('/^([0-9]+)[\s]?([a-zA-Z]+)?$/', $expireTime, $m);

            $time = $m[1];
            $type = $m[2] ?? null;
        } else {
            $time = $expireTime;
            $type = null;
        }


        if ($type === 'min') {
            // $expire = Carbon::now()->addMinutes($time);
            $expire = $time * 60;
        } elseif ($type === 'hour') {
            // $expire = Carbon::now()->addHours($time);
            $expire = $time * 60 * 60;
        } elseif ($type === 'day') {
            // $expire = Carbon::now()->addDays($time);
            $expire = $time * 60 * 60 * 24;
        } elseif ($type === 'week') {
            // $expire = Carbon::now()->addWeeks($time);
            $expire = $time * 60 * 60 * 24 * 7;
        } elseif ($type === 'month') {
            // $expire = Carbon::now()->addMonths($time);
            $expire = strtotime('+ 1 month') - time();
        } else {
            $expire = $time;
        }

        return $expire;
    }
}

