<?php


namespace Anon\Classes;

use Anon\Models\ConfigModel;

class Config extends AnonInstance
{
    function set($key, $value)
    {
        $value = json_encode($value);
        ConfigModel::updateOrCreate(
            ['config_key' => $key],
            ['config_value' => $value]
        );
    }

    function get($key, $default = null)
    {
        $cfg = ConfigModel::where(['config_key' => $key])->first();

        if (empty($cfg)) {
            $o = $default;
        } else {
            $o = $cfg->config_value;
            $o = json_decode($o);
        }

        if (is_string($o)) {
            $o = trim($o);
        }

        return $o;
    }

    function delete($key)
    {
        ConfigModel::where(['config_key' => $key])->delete();
    }
}
