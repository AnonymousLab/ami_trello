<?php


namespace Anon\Classes;

use Illuminate\Support\Facades\Http;

class TrelloApi extends AnonInstance
{
    private $apiUrl = [
        'member' => 'https://api.trello.com/1/',
        'board' => 'https://api.trello.com/1/boards/{id}',
        'list' => 'https://api.trello.com/1/lists/{id}',
        'card' => 'https://api.trello.com/1/cards/{id}',
    ];
    private $query = [];
    private $cardCache = [];


    function __construct()
    {
        $this->query = [
            'key' => Config::gi()->get('trello_key'),
            'token' => Config::gi()->get('trello_token'),
        ];
    }


    function apiUrl($type, $replace = '', $append = '')
    {
        $url = $this->apiUrl[$type];

        if (!empty($append)) {
            $url .= '/' . $append;
        }

        if (!empty($replace)) {
            $url = str_replace('{id}', $replace, $url);
        }

        return $url;
    }


    function getQuery($query = null)
    {
        $o = $this->query;
        if (!empty($query)) {
            $o = array_merge($o, $query);
        }
        return $o;
    }


    /**
     * 회원정보 가져오기
     *
     * @param  string  $boardId
     * @return array
     */
    function getMemberList($boardId = '')
    {
        $cacheKey = sha1("getMemberList_{$boardId}");
        if (isset($this->cardCache[$cacheKey])) {
            return $this->cardCache[$cacheKey];
        }

        $query = $this->getQuery();
        $url = $this->apiUrl('board', $boardId, 'members');

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get($url, $query);

        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        $this->cardCache[$cacheKey] = $rt;

        return $rt;
    }


    /**
     * board 의 list 추출
     *
     * @param $boardId
     * @return array
     */
    function getLists($boardId)
    {
        $query = $this->getQuery();
        $url = $this->apiUrl('board', $boardId, 'lists');

        $response = Http::get($url, $query);

        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        return $rt;
    }


    /**
     * list 의 card 추출
     *
     * @param $listId
     * @return array
     */
    function getCardsInList($listId)
    {
        $query = $this->getQuery();
        $url = $this->apiUrl('list', $listId, 'cards');

        $response = Http::get($url, $query);
        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        return $rt;
    }

    function getListAction($listId)
    {
        $cacheKey = sha1("getListAction_{$listId}");
        if (isset($this->cardCache[$cacheKey])) {
            return $this->cardCache[$cacheKey];
        }

        $query = $this->getQuery();
        $url = $this->apiUrl('list', $listId, 'actions');

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get($url, $query);
        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        $this->cardCache[$cacheKey] = $rt;

        return $rt;
    }


    /**
     * 카드 정보를 가져온다.
     *
     * @param $cardId
     * @return array
     */
    function getCardInfo($cardId)
    {
        $cacheKey = sha1("getCardInfo_{$cardId}");
        if (isset($this->cardCache[$cacheKey])) {
            return $this->cardCache[$cacheKey];
        }

        $query = $this->getQuery();
        $url = $this->apiUrl('card', $cardId);

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get($url, $query);
        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        $this->cardCache[$cacheKey] = $rt;

        return $rt;
    }


    /**
     * 특정 리스트로 이동 된 시간을 가져온다.
     *
     * @param $cardId
     * @param $listId
     * @return string
     */
    function getCardMovedTime($cardId, $listId = null)
    {
        $cacheKey = sha1("getCardMovedTime_{$cardId}_{$listId}");
        if (isset($this->cardCache[$cacheKey])) {
            return $this->cardCache[$cacheKey];
        }

        $query = $this->getQuery();
        $url = $this->apiUrl('card', $cardId, 'actions');

        $query['filter'] = 'updateCard:idList';

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get($url, $query);
        $status = $response->status();

        $rt = null;

        if ($status === 200) {
            $jsonRt = $response->json();
            foreach ($jsonRt as $k => $v) {
                if (!empty($listId) && $v['data']['card']['idList'] === $listId) {
                    $rt = $v['date'];
                    break;
                } else {
                    $rt = $v['date'];
                    break;
                }
            }
        }

        try {
            if (!empty($rt)) {
                $dt = new \DateTime($rt);
                $dt->setTimezone(new \DateTimeZone('Asia/Seoul'));
                $rt = $dt->format('Y-m-d H:i:s');
            }

        } catch (\Throwable $e) {
            $rt = null;
        }

        $this->cardCache[$cacheKey] = $rt;

        return $rt;
    }


    /**
     * Trello ID 로 생성 시간을 가져온다.
     *
     * @param $cardId
     * @return string
     */
    function getCreateTime($cardId)
    {
        $dt = new \DateTime();
        $time = substr($cardId, 0, 8);
        $time2 = hexdec($time);
        $dt->setTimestamp($time2);
        $dt->setTimezone(new \DateTimeZone('Asia/Seoul'));

        return $dt->format('Y-m-d H:i:s');
    }


    /**
     * 카드삭제
     *
     * @param $cardId
     * @return array
     */
    function deleteCard($cardId)
    {
        $query = $this->getQuery();
        $url = $this->apiUrl('card', $cardId);


        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->delete($url, $query);

        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        return $rt;
    }


    /**
     * card 업데이트
     *
     * @param $cardId
     * @param $data
     * @return array
     */
    function updateCard($cardId, $data)
    {
        $query = $this->getQuery();
        $url = $this->apiUrl('card', $cardId);

        $query = array_merge($query, $data);

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->put($url, $query);

        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        return $rt;
    }


    /**
     * list 의 모든 card 를 archive 시킴
     *
     * @param $listId
     */
    function moveArchiveList($listId)
    {
        $query = $this->getQuery();
        $url = $this->apiUrl('list', $listId, 'lists');

        $response = Http::post($url, $query);
        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }
    }


    /**
     * archive 된 cards 추출
     *
     * @param $boardId
     * @return array
     */
    function getArchiveCards($boardId)
    {
        $query = $this->getQuery();
        $query['filter'] = 'closed';
        $url = $this->apiUrl('board', $boardId, 'cards');

        $response = Http::get($url, $query);
        $status = $response->status();

        if ($status === 200) {
            $rt = [true, $response->json(), null];
        } else {
            $rt = [false, null, $response->serverError()];
        }

        return $rt;
    }
}
