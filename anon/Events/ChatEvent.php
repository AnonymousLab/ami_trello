<?php

namespace Anon\Events;

use App\Events\Event;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
// use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ChatEvent extends Event implements ShouldBroadcastNow
{
    public $message;
    private $roomId;

    /**
     * Chat constructor.
     * @param $data
     * @param $message
     */
    public function __construct($data, $message)
    {
        $this->message = $message;
        $this->roomId = $data->roomId;
    }

    /**
     * @return PresenceChannel
     */
    public function broadcastOn()
    {
        return new PresenceChannel("ott.chat.{$this->roomId}");
    }

    public function broadcastAs()
    {
        return 'ott.chat';
    }
}
