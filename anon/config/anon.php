<?php

return [
    'trello_key' => env('TRELLO_KEY'),
    'trello_token' => env('TRELLO_TOKEN'),
    'ip_allow' => env('IP_ALLOW'),
];
