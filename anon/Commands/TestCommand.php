<?php


namespace Anon\Commands;

use Illuminate\Console\Command;

class TestCommand extends Command
{
    protected $signature = 'zzz:command';

    protected $description = 'Test Command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('test command');
    }
}
