<?php


namespace Anon\Commands;

use Anon\Classes\Config;
use Anon\Classes\Trello;
use Anon\Classes\TrelloApi;
use Anon\Models\ListModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TrelloCommand extends Command
{
    protected $signature = 'trello {args1?}';

    protected $description = 'Trello Command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $args1 = $this->argument('args1');

        if (!empty($args1)) {
            $choice = $args1;
        } else {
            $choice = $this->choice('Config Setting',
                [
                    'Exists Menu',
                    '1. Member Sync',
                    '2. Sync Lists',
                    '3. Set "To Do" list',
                    '4. Set "Doing" list',
                    '5. Set "Done" list',
                    '6. Sync "To Do" Cards',
                    '7. Sync "Doing" Cards',
                    '8. Sync "Done" Cards',
                    '9. Sync "All" Cards',
                ], 0
            );

            $choice = substr($choice, 0, 1);
        }

        $choice = (int) $choice;

        if ($choice === 1) {
            Trello::gi()->memberSync();
            $this->info('finish member sync');
        } elseif ($choice === 2) {
            $this->syncLists();
            $this->info('finish list sync');
        } elseif ($choice === 3) {
            $this->setToDoList();
        } elseif ($choice === 4) {
            $this->setDoingList();
        } elseif ($choice === 5) {
            $this->setDoneList();
        } elseif ($choice === 6) {
            Trello::gi()->syncCards('todo');
        } elseif ($choice === 7) {
            Trello::gi()->syncCards('doing');
        } elseif ($choice === 8) {
            Trello::gi()->syncCards('done');
        } elseif ($choice === 9) {
            Trello::gi()->syncCards(null);
            $this->info('finish card sync');
        }
    }

    function syncLists()
    {
        $boardId = Trello::gi()->getBoardId();
        [$statue, $lists, $msg] = TrelloApi::gi()->getLists($boardId);

        if (!$statue) {
            $this->error($msg);
            return null;
        }

        ListModel::truncate();
        foreach ($lists AS $li) {
            $li2 = snakeKeys($li);
            ListModel::create($li2);
        }

        $this->info('Sync Lists');
    }

    function setDoingList()
    {
        $boardId = Trello::gi()->getBoardId();
        [$statue, $lists, $msg] = TrelloApi::gi()->getLists($boardId);

        if (!$statue) {
            $this->error($msg);
            return null;
        }

        $menus = ['Exists'];
        $idBoard = [];
        foreach ($lists AS $li) {
            $tmp = "{$li['name']} ({$li['id']})";
            $menus[] = $tmp;
            $idBoard[$tmp] = $li['id'];
        }

        $choice = $this->choice('Select Doing List', $menus, 0);

        if ($choice === 'Exists') {
            return null;
        }

        Config::gi()->set('trello_doing_list_id', $idBoard[$choice]);

        $this->info('Set Doing List');
    }

    function setToDoList()
    {
        $boardId = Trello::gi()->getBoardId();
        [$statue, $lists, $msg] = TrelloApi::gi()->getLists($boardId);

        if (!$statue) {
            $this->error($msg);
            return null;
        }

        $menus = ['Exists'];
        $idBoard = [];
        foreach ($lists AS $li) {
            $tmp = "{$li['name']} ({$li['id']})";
            $menus[] = $tmp;
            $idBoard[$tmp] = $li['id'];
        }

        $choice = $this->choice('Select To-Do List', $menus, 0);

        Config::gi()->set('trello_to_do_list_id', $idBoard[$choice]);

        $this->info('Set To Do List');
    }

    function setDoneList()
    {
        $boardId = Trello::gi()->getBoardId();
        [$statue, $lists, $msg] = TrelloApi::gi()->getLists($boardId);

        if (!$statue) {
            $this->error($msg);
            return null;
        }

        $menus = ['Exists'];
        $idBoard = [];
        foreach ($lists AS $li) {
            $tmp = "{$li['name']} ({$li['id']})";
            $menus[] = $tmp;
            $idBoard[$tmp] = $li['id'];
        }

        $choice = $this->choice('Select Done List', $menus, 0);

        Config::gi()->set('trello_done_list_id', $idBoard[$choice]);

        $this->info('Set Doen List');
    }
}
