<?php

namespace Anon\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $card_id
 * @property string $member_id
 */
class MemberCardModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_card';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['card_id', 'member_id'];

    public $timestamps = false;
}
