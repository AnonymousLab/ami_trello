<?php

namespace Anon\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $config_key
 * @property string $config_value
 */
class ConfigModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['config_key', 'config_value'];

    public $timestamps = false;
}
