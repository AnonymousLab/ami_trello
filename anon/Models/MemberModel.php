<?php

namespace Anon\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $username
 * @property string $full_name
 */
class MemberModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id', 'username', 'full_name'];

    public $timestamps = false;


    public function cards()
    {
        /*
SELECT
  `trello_card`.*,
  `trello_member_card`.`member_id` AS `laravel_through_key`
FROM
  `trello_card`
  INNER JOIN `trello_member_card`
    ON `trello_member_card`.`card_id` = `trello_card`.`id`
WHERE `trello_member_card`.`member_id` = ?
         */
        return $this->hasManyThrough(
            'Anon\Models\CardModel', // 최종 테이블
            'Anon\Models\MemberCardModel', // 중간 테이블
            'member_id', // MemberCardModel.member_id 과 CardModel.id 매칭
            'id', // CardModel
            'id', // MemberModel local key
            'card_id' // MemberCardModel
        )->orderBy('id_short');
    }
}
