<?php

namespace Anon\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property boolean $closed
 * @property string $desc
 * @property string $id_board
 * @property string $id_list
 * @property int $id_short
 * @property string $short_link
 * @property string $short_url
 * @property boolean $is_hidden
 * @property string $time_create
 * @property string $time_start
 * @property string $time_done
 * @property string $due
 * @property boolean $due_complete
 */
class CardModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'closed', 'desc', 'id_board', 'id_list', 'id_short', 'short_link', 'short_url', 'is_hidden', 'time_create', 'time_start', 'time_done', 'due', 'due_complete'];

    public $timestamps = false;

    public function cardListWithMember()
    {
        return $this->hasMany('Anon\Models\MemberCardModel', 'card_id', 'id');;
    }
}
