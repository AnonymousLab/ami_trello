<?php

namespace Anon\Models;

use Anon\Classes\Uid;
use Anon\Extend\Collection;
use Anon\Util\Lib;

trait TraitModel
{
    public static function getTableName()
    {
        $prefix = \DB::getTablePrefix();
        return $prefix . with(new static)->getTable();
        // return $dis->getTablePrefix() . $dis->getTable();
    }
}
