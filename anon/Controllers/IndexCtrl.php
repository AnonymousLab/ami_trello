<?php


namespace Anon\Controllers;

use Anon\Classes\ACache;
use Anon\Classes\Config;
use Anon\Classes\Trello;
use Anon\Classes\TrelloApi;
use Anon\Classes\Util;
use Anon\Models\CardModel;
use Anon\Models\MemberModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IndexCtrl extends Controller
{
    public function __construct()
    {
        $ipAllow = Config::gi()->get('allow_ip');
        $ipAllowArr = explode(',', $ipAllow);
        $ip = $_SERVER['REMOTE_ADDR'];
        if (!empty($ipAllow)
            && !in_array($ip, $ipAllowArr)
            && !preg_match("/^192\.168\./", $ip)
        ) {
            echo $_SERVER['REMOTE_ADDR'];
            exit;
        }
    }


    /**
     * index page
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    function index(Request $request)
    {
        // '2020-06-23T12:49:20.000';
        // $bodId = '5ee6ce9a31a3c9675eafa5c0';
        // $rt = TrelloApi::gi()->getMemberList('5ee6ce9a31a3c9675eafa5c0');
        // $rt = TrelloApi::gi()->getLists('fjmbszqo');
        // $rt = TrelloApi::gi()->getCardsInList('5ee6ce9a31a3c9675eafa5c7');
        // $rt = TrelloApi::gi()->getCardsInList('5ee6ce9a31a3c9675eafa5c1');
        // $rt = TrelloApi::gi()->getCardInfo('5ee6ce9a31a3c9675eafa5c0'); // 보드 액션
        // $rt = TrelloApi::gi()->getListAction('5ee6ce9a31a3c9675eafa5c3'); // list 액션
        // $rt = TrelloApi::gi()->getCardInfo('5ee7147df2a9537c8abf099d'); // card 액션
        // $rt = TrelloApi::gi()->getCardMovedTime('5ee986b9da3a485514a96ce0', '5ee6ce9a31a3c9675eafa5c7'); // card 액션
        // dd($rt);
        // $rt = Trello::gi()->completeCard('5ef1c3456ecd50248cad985c');
        // dd($rt);
        // $rt = TrelloApi::gi()->deleteCard('5ef1c3456ecd50248cad985c');
        // dd($rt);

        // $t = MemberModel::first()->cards()->where('id_list', '5ee6ce9a31a3c9675eafa5c4')->get();

        if (!empty(Util::gi()->checkConfigRequired())) {
            $url = route('index.setting');;
            return response('<script>alert("환경 설정이 설정되지 않아 Setting 페이지로 이동합니다.");location.href="'.$url.'"</script>');
        }

        $listIds = Util::gi()->getLists();

        // list id 추출
        $usedListIds = Util::gi()->getUsedListIds();

        // member 추출
        $members = Util::gi()->getMembers();

        // ---------------------------
        // card 추출
        // ---------------------------
        $cards = [];
        foreach ($members AS $mb) {
            foreach ($usedListIds AS $type => $liId) {
                $cards[$mb->id][$type] = $mb->cards->where('id_list', $liId)->all();
            }
        }

        $ganttArgs = compact('members', 'cards', 'usedListIds');
        $gantt = Util::gi()->ganttJson($ganttArgs);
        $viewArgs['gantt'] = $gantt;
        // ---------------------------

        $memberArr = [];
        if (!empty($members)) {
            $memberArr = $members->keyBy('id')->toArray();
        }

        // ---------------------------
        // 완료 된 card 추출
        // ---------------------------
        $cardList = Util::gi()->getCardList();

        // compact 이 귀찮아서...
        $viewArgs = get_defined_vars();

        return view('index.index', $viewArgs);
    }


    /**
     * card list 페이지
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    function cardList(Request $request)
    {
        if (!empty(Util::gi()->checkConfigRequired())) {
            $url = route('index.setting');;
            return response('<script>alert("환경 설정이 설정되지 않아 Setting 페이지로 이동합니다.");location.href="'.$url.'"</script>');
        }

        $get = $request->all();
        $get['limit'] = 50;

        $listIds = Util::gi()->getLists();
        $members = Util::gi()->getMembers();
        $memberArr = [];
        if (!empty($members)) {
            $memberArr = $members->keyBy('id')->toArray();
        }

        // list id 추출
        $usedListIds = Util::gi()->getUsedListIds();

        // to do, doing, done list id 추출
        $todoDoingDoneList = Util::gi()->getTodoDoingDoneList();

        $get['is_hidden'] = 0;
        $cardList = Util::gi()->getCardList($get);

        // compact 이 귀찮아서...
        $viewArgs = get_defined_vars();

        return view('index.card-list', $viewArgs);
    }


    /**
     * 각종 post action 처리
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function action(Request $request)
    {
        $type = $request->input('type');
        $cardId = $request->input('card_id');

        $rt = [false, null, '-_-'];

        if ($type === 'trello_sync') {
            Trello::gi()->syncCards(null);
            return redirect(url()->previous());

        } elseif ($type === 'sync') {
            $rt = Trello::gi()->syncOneCard($cardId);
        } elseif ($type === 'delete') {
            $rt = Trello::gi()->deleteCard($cardId);
        } elseif ($type === 'complete') {
            $rt = Trello::gi()->completeCard($cardId);
        }

        return response()->json($rt);
    }


    /**
     * setting 페이지
     *
     * @param  Request  $request
     * @param  Config  $config
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function setting(Request $request, Config $config)
    {
        $trelloKey = $config->get('trello_key');
        $trelloToken = $config->get('trello_token');
        $allowIp = $config->get('allow_ip');
        $trelloBoardId = $config->get('trello_board_id');
        $trelloListArr = $config->get('trello_list');
        $trelloUsedList = $config->get('trello_used_list', []);
        $trelloTodoListId = $config->get('trello_todo_list_id', []);
        $trelloDoingListId = $config->get('trello_doing_list_id', []);
        $trelloDoneListId = $config->get('trello_done_list_id', []);

        $requiredConfig = !Util::gi()->checkConfigRequired();

        // member 추출
        $members = Util::gi()->getMembers();

        // compact 이 귀찮아서...
        $viewArgs = get_defined_vars();

        return view('index.setting', $viewArgs);
    }


    /**
     * setting action 처리
     *
     * @param  Request  $request
     * @param  Config  $config
     * @return \Illuminate\Http\RedirectResponse
     */
    function settingUpdate(Request $request, Config $config)
    {
        $syncMember = $request->input('sync_member');
        $syncArchiveCards = $request->input('sync_archive_cards');
        $syncList = $request->input('sync_list');

        if (!empty($syncMember)) {
            Trello::gi()->memberSync();
            return redirect()->route('index.setting');
        } elseif (!empty($syncArchiveCards)) {
            Trello::gi()->syncCardsExec('sync_archive_cards');
            return redirect()->route('index.setting');
        } elseif (!empty($syncList)) {
            Trello::gi()->syncList();
            return redirect()->route('index.setting');
        }

        $trelloKey = $request->input('trello_key');
        $trelloToken = $request->input('trello_token');
        $allowIp = $request->input('allow_ip');
        $boardId = $request->input('board_id');
        $usedList = $request->input('used_list', []);
        $todoListId = $request->input('todo_list');
        $doneListId = $request->input('done_list');
        $doingListId = $request->input('doing_list');

        $config->set('trello_key', $trelloKey);
        $config->set('trello_token', $trelloToken);
        $config->set('allow_ip', $allowIp);

        // 이전 board id 추출
        $oldBoardId = $config->get('trello_board_id');

        // board id 저장
        if (!empty($boardId) && $boardId !== $oldBoardId) {
            // config 저장
            $config->set('trello_board_id', $boardId);

            // board id 가 저장 되면 리스트 가져와서 저장
            Trello::gi()->syncList();
        }

        // 사용할 list id 저장
        $config->set('trello_used_list', $usedList);

        // to do, done list 저장
        if (!empty($todoListId) && !empty($doneListId) && $todoListId === $doneListId) {
            return redirect()->route('index.setting')->withErrors('todo 와 done 리스트가 같음.');
        } elseif (!empty($todoListId) && !empty($doingListId) && $todoListId === $doingListId) {
            return redirect()->route('index.setting')->withErrors('todo 와 doing 리스트가 같음.');
        } elseif (!empty($doneListId) && !empty($doingListId) && $doneListId === $doingListId) {
            return redirect()->route('index.setting')->withErrors('done 와 doing 리스트가 같음.');
        } else {
            $config->set('trello_todo_list_id', $todoListId);
            $config->set('trello_done_list_id', $doneListId);
            $config->set('trello_doing_list_id', $doingListId);
        }

        return redirect()->route('index.setting');
    }



    function trelloReturn(Request $request)
    {
        dump($request->all());
        dump(file_get_contents('php://input'));
    }
}
