<?php

namespace Anon\Controllers;

use Anon\Classes\CryptoJs;
use Anon\Classes\User;
use Illuminate\Routing\Controller as BaseController;
use JWTAuth;

class Controller extends BaseController
{
    protected function returnJsonNormal($data = null, $statusCode = 200)
    {
        return response()->json($data, $statusCode)->withHeaders([
            'Cache-Control' => 'no-cache, nocache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'no-cache',
            'Expires' => 'Sat, 01 Jan 1990 00:00:00 GMT',
        ]);
    }


    protected function returnJson($status = 0, $data = null, $msg = null, $statusCode = 200)
    {
        if (is_array($status)) {
            $data = $status[1] ?? null;
            $msg = $status[2] ?? null;
            $statusCode = $status[3] ?? 200 ?: 200;
            $status = $status[0] ?? false;
        }

        if (!$status && empty($statusCode)) {
            $statusCode = 400;
        }

        $statusStr = ($status) ? 'success' : 'error';

        $json = [
            'status'  => $statusStr,
        ];


        $json['data'] = $data;

        if (is_array($data) && isset($data['--payload'])) {
            unset($data['--payload'], $json['data']['--payload']);

            foreach ($data AS $k => $v) {
                $json[$k] = $v;
            }
        }

        if (!empty($msg)) {
            $json['message'] = $msg;
        }

        $headers = [
            // response.setHeader("Set-Cookie", "HttpOnly;Secure;SameSite=Strict");
            // 'Set-Cookie' => 'HttpOnly;Secure;SameSite=None',
            'Cache-Control' => 'no-cache, nocache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'no-cache',
            'Expires' => 'Sat, 01 Jan 1990 00:00:00 GMT',
            // 'Content-Type' => 'text/json; charset=utf-8',
        ];

        // 구형 브라우저
        if (preg_match('/(?i)msie [5-9]/', $_SERVER['HTTP_USER_AGENT'])) {
//            $headers['Content-Type'] = 'text/json; charset=utf-8';
            $headers['Content-Type'] = 'text/plain; charset=utf-8';
//            dump('dd');
//            return response(json_encode($json), $statusCode)->withHeaders($headers);
        }

        // Log::info($_SERVER['HTTP_AUTHORIZATION']);
        if(JWTAuth::check()) {
            $iat = JWTAuth::getClaim('iat');
            $exp = JWTAuth::getClaim('exp');
            $time = time();

            // if ($time - $iat > (60 * 15)) {
            if ($exp - $time < (60 * 40)) {
                $token = User::gi()->refreshToken();
                $expired = time() + JWTAuth::factory()->getTTL() * 60;
                $headers['Anon-Token'] = "{$token}||{$expired}";
                // Log::info($token);
            }
        }


        return response()->json($json, $statusCode)->withHeaders($headers);
    }

    //--------------------------------------------------------------------------

    protected function returnJsonEnc($status = 0, $data = null, $msg = null, $statusCode = 200)
    {
        $ctrl = $this->returnJson($status, $data, $msg, $statusCode);
        if (env('APP_DEBUG') === true) {
            return $ctrl;
        }

        $headers = $ctrl->headers->all();
        $json = $ctrl->getOriginalContent();
        $statusCode = $ctrl->status();

        $tmp = json_encode($json['data']);
        $tmp = CryptoJs::gi()->encrypt($tmp, config('anon.crypto_js'));
        unset($json['data']);
        $json['DATA'] = $tmp;

        return response()->json($json, $statusCode)->withHeaders($headers);
    }

    //--------------------------------------------------------------------------

    protected function checkAjax()
    {
//        if (!preg_match('/(?i)msie [5-9]/', $_SERVER['HTTP_USER_AGENT']) && !request()->ajax()
        if (!request()->ajax()
        ) {
            abort(400, '^_^');
        }
    }

    //--------------------------------------------------------------------------

    protected function checkApi()
    {
        if (!request()->ajax()) {
            abort(400, '^_^');
        }
    }

    //--------------------------------------------------------------------------

    protected function redirectAlert($message = '', $url = '/')
    {
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $message = addslashes($message);

        $header = [
            'Cache-Control' => 'no-cache, nocache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'no-cache',
            'Expires' => 'Sat, 01 Jan 1990 00:00:00 GMT',
        ];

        $reUrl = '';
        if ($url === false) {
            $reUrl = "window.close();";

        } elseif ($url !== null) {
            $reUrl = "location.replace('{$url}');";
        }


        return response("<script>alert('{$message}');{$reUrl}</script>")->withHeaders($header);
        // abort(404, $message);
    }
}


