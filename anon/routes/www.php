<?php
/** @var Illuminate\Support\Facades\Route $router */

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});

$router->get('/', ['as' => 'index', 'uses' => 'IndexCtrl@index']);
$router->get('list', 'IndexCtrl@cardList')->name('index.card-list');
$router->post('action', 'IndexCtrl@action')->name('index.action');

$router->get('setting', 'IndexCtrl@setting')->name('index.setting');
$router->post('setting', 'IndexCtrl@settingUpdate')->name('index.setting-update');


$router->get('trello_auth_return', 'IndexCtrl@trelloReturn')->name('index.trello-return');
