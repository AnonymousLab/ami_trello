<?php
/*
  php artisan krlove:generate:model MemberModel --table-name=user --output-path=/home/vagrant/code/trello/anon/Models --namespace=Anon\\Model
  php artisan krlove:generate:model ListModel --table-name=list --output-path=/home/vagrant/code/trello/anon/Models --namespace=Anon\\Model
  php artisan krlove:generate:model CardModel --table-name=card --output-path=/home/vagrant/code/trello/anon/Models --namespace=Anon\\Model
  php artisan krlove:generate:model MemberCardModel --table-name=member_card --output-path=/home/vagrant/code/trello/anon/Models --namespace=Anon\\Model
  php artisan krlove:generate:model ConfigModel --table-name=config --output-path=/home/vagrant/code/trello/anon/Models --namespace=Anon\\Model
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SettingDbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Trello 회원정보
        Schema::create('member', function (Blueprint $table) {
            $table->string('id', 24)->primary();
            $table->string('username', 20)->default('');
            $table->string('full_name', 20)->default('');

            $table->index('username');
            $table->index('full_name');
        });

        // Trello 리스트 정보
        Schema::create('list', function (Blueprint $table) {
            $table->string('id', 24)->primary();
            $table->string('name', 100)->default('');
            $table->tinyInteger('closed')->default(0);
            $table->string('id_board', 24)->default('');

            $table->index('name');
            $table->index('closed');
            $table->index('id_board');
        });

        // Trello 카드 정보
        Schema::create('card', function (Blueprint $table) {
            $table->string('id', 24)->primary()->comment('card id');
            $table->string('name', 100)->default('')->comment('card title');
            $table->tinyInteger('closed')->default(0);
            $table->text('desc')->nullable()->comment('card description');
            $table->string('id_board', 24)->default('');
            $table->string('id_list', 24)->default('');
            $table->integer('id_short')->default(0);
            $table->string('short_link', 20)->default('');
            $table->string('short_url', 100)->default('');
            $table->integer('due_complete')->default(0);
            $table->dateTime('due', 0)->nullable();

            // 아래는 trello 에 없는 정보
            $table->tinyInteger('is_hidden')->default(0);
            $table->dateTime('time_create', 0)->nullable();
            $table->dateTime('time_start', 0)->nullable();
            $table->dateTime('time_done', 0)->nullable();

            $table->index('name');
            $table->index('closed');
            $table->index('id_board');
            $table->index('id_list');
            $table->index('id_short');
            $table->index('is_hidden');
            $table->index('time_create');
            $table->index('time_start');
            $table->index('time_done');
        });

        // Trello 카드와 멤버를 매칭 시키는 테이블
        Schema::create('member_card', function (Blueprint $table) {
            $table->id();
            $table->string('card_id', 24)->default('');
            $table->string('member_id', 24)->default('');

            $table->index('card_id');
            $table->index('member_id');
        });

        Schema::create('config', function (Blueprint $table) {
            $table->id();
            $table->string('config_key', 30)->default('');
            $table->text('config_value')->nullable();

            $table->index('config_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        Schema::dropIfExists('list');
        Schema::dropIfExists('card');
        Schema::dropIfExists('member_card');
        Schema::dropIfExists('config');
    }
}
