<?php
/**
 * Created by PhpStorm.
 * User: kangtaejin
 * Date: 2017. 5. 7.
 * Time: PM 5:21
 */

namespace Anon\Providers;


use Anon\Extend\JWTAuth2;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Tymon\JWTAuth\JWTAuth;


class AnonServiceProvider extends ServiceProvider
{
    protected $seedHook = false;
    protected $anonPath = __DIR__ . '/../';


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //------------------------------
        // Config
        //------------------------------
        if (env('APP_NAME') !== null) {
            $configPath = "{$this->anonPath}config/";
            $configs = [
                'jwt', 'view', 'auth', 'cache', 'database', 'session', 'filesystems', 'anon'
            ];
            foreach ($configs AS $cfg) {
                if (is_file("{$configPath}{$cfg}.php")) {
                    // -----------------------
                    // lumen 특성 시작
                    // $this->loadedConfigurations[$name] = true; 를 만들기 위해 configure(key, null) 를 했음.
                    // 한번 부른 것을 다시 불러오지 않게 하기 위함임. 이래야만 다른 provider 에서 엎어씌기가 안됨.
                    // $this->app->configure($cfg, null);
                    // -----------------------

                    $this->app['config']->set($cfg, require "{$configPath}{$cfg}.php");

                    // 아래 mergeConfigFrom 를 사용하지 않는 이유는 기존 설정값이 우선시 되고 덮어 씌기가 되지 않는다..
                    // $this->mergeConfigFrom("{$configPath}{$cfg}.php", $cfg);
                }
            }
        }

    }


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $anonPath = $this->anonPath;

        // view 경로 설정 (config/view.php 에서 정의해서 주석 처리)
        // $this->loadViewsFrom("{$anonPath}view", 'view');
        // $this->loadViewsFrom("{$anonPath}view/_layout", 'layout');

        // migrate path
        $this->loadMigrationsFrom("{$anonPath}database/migrations");

        // lang path
        $this->loadTranslationsFrom("{$anonPath}lang", 'anon');

        // route
        // $this->loadRoutesFrom("{$anonPath}/routes/routes.php");

        // command
        $this->commands([
            \Anon\Commands\TestCommand::class,
            \Anon\Commands\TrelloCommand::class,
        ]);
        if ((\PHP_SAPI === 'cli' || \PHP_SAPI === 'phpdbg') && $this->app->runningInConsole()) {
            // 시드 설치
            $this->registerSeedsFrom();
        }
    }


    /**
     * Anon 패키지 안의 시드만 설치하기 위해서 만듬.
     *
     * @return |null
     */
    protected function registerSeedsFrom()
    {
        $command = Request::server('argv', null);
        if (!is_array($command)) {
            return null;
        }

        if ($command[0] !== 'artisan'
            || empty($command[1])
            || (
                !(preg_match("/^migrate(:refresh)?/i", $command[1]) && in_array('--seed', $command))
                && $command[1] !== 'db:seed'
            )
        ) {
            return null;
        }

        $this->seedHook = true;
    }


    protected function executeSeeds()
    {
        $path = "{$this->anonPath}database/seeds";
        $command = Request::server('argv', null);

        foreach (glob("$path/*.php") as $filename)
        {
            // include $filename;
            $className = pathinfo($filename, PATHINFO_FILENAME);
            $commandOpts = [
                '--class' => "Anon\\database\\seeds\\{$className}",
            ];

            if (in_array('--force', $command)) {
                $commandOpts['--force'] = true;
            }

            Artisan::call('db:seed', $commandOpts);
        }

        echo "\033[32mPackage Database seeding completed successfully.\033[0m";
        echo PHP_EOL;
    }


    function __destruct() {
        if ($this->seedHook === true) {
            $this->executeSeeds();
        }
    }
}
