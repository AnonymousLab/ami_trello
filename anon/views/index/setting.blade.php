@extends('layouts.default')

@section('content')
    <div class="container pt-2">
        @include('index.index-tab')

        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                {{ $error }}
                </div>
            @endforeach
        @endif
        <div>
            <form action="{{ route('index.setting-update') }}" method="post">
                @csrf

                <div class="form-group row">
                    <label for="input_key" class="col-sm-3 col-form-label">
                        Trello Key
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="trello_key" class="form-control" id="input_key" value="{{ $trelloKey ?? '' }}">
                        발급: <a href="https://trello.com/app-key" target="_blank">https://trello.com/app-key</a>
                        <br>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="input_token" class="col-sm-3 col-form-label">
                        Trello Token
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="trello_token" class="form-control" id="input_token" value="{{ $trelloToken ?? '' }}">
                        발급: <a href="https://trello.com/app-key" target="_blank">https://trello.com/app-key</a>
                        <br>
                        generate a Token. 이라는 글자가 보일 것임. Token 단어를 클릭
                    </div>
                </div>

                <div class="form-group row">
                    <label for="input1" class="col-sm-3 col-form-label">
                        Trello Board Id
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="board_id" class="form-control" id="input1" value="{{ $trelloBoardId ?? '' }}">
                        트렐로 주소가 https://trello.com/b/fjmbszqo/wcms 이런식임. fjmbszqo 입력
                    </div>
                </div>

                @if ($trelloBoardId && $trelloListArr)
                <div class="form-group row">
                    <label for="input1" class="col-sm-3 col-form-label">
                        Trello List
                        <br>
                        (동기화할 list를 산택)
                    </label>
                    <div class="col-sm-9">
                        @foreach ($trelloListArr AS $li)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="used_list[]"
                                       id="checkbox_{{ $loop->index }}"
                                       value="{{ $li->id }}"
                                       {{ in_array($li->id, $trelloUsedList) ? 'checked' : '' }}
                                >
                                <label class="form-check-label" for="checkbox_{{ $loop->index }}">{{ $li->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif

                @if ($trelloBoardId && $trelloUsedList)
                    <div class="form-group row">
                        <label for="select_1" class="col-sm-3 col-form-label">
                            ToDo list 지정
                        </label>
                        <div class="col-sm-9">
                            <select id="select_1" class="form-control" name="todo_list">
                                @foreach ($trelloUsedList AS $li)
                                <option value="{{ $li }}"
                                    {{ $li === $trelloTodoListId ? 'selected' : '' }}
                                >{{ \Anon\Classes\Util::gi()->getListName($li) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="select_2" class="col-sm-3 col-form-label">
                            Doing list 지정
                        </label>
                        <div class="col-sm-9">
                            <select id="select_2" class="form-control" name="doing_list">
                                @foreach ($trelloUsedList AS $li)
                                    <option value="{{ $li }}"
                                        {{ $li === $trelloDoingListId ? 'selected' : '' }}
                                    >{{ \Anon\Classes\Util::gi()->getListName($li) }}</option>
                                @endforeach
                            </select>
                            (이 리스트로 이동된 시간을 작업 시작일로 저장)
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="select_3" class="col-sm-3 col-form-label">
                            Done list 지정
                        </label>
                        <div class="col-sm-9">
                            <select id="select_3" class="form-control" name="done_list">
                                @foreach ($trelloUsedList AS $li)
                                    <option value="{{ $li }}"
                                            {{ $li === $trelloDoneListId ? 'selected' : '' }}
                                    >{{ \Anon\Classes\Util::gi()->getListName($li) }}</option>
                                @endforeach
                            </select>
                            (이 리스트로 이동된 시간을 작업 완료일로 저장)
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <label for="input_ip_arr" class="col-sm-3 col-form-label">
                        접근허용IP
                    </label>
                    <div class="col-sm-9">
                        <textarea name="allow_ip" id="input_ip_arr" class="form-text w-100" rows="5">{{$allowIp}}</textarea>
                        (쉼표로 구분)
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">
                    설정 저장
                    </label>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary">설정 저장</button>
                    </div>
                </div>
            </form>


            @if ($trelloBoardId)
                <hr>
                <form action="{{ route('index.setting-update') }}" method="post">
                    @csrf
                    <input type="hidden" name="sync_list" value="1">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">
                            List 동기화
                        </label>
                        <div class="col-sm-9">
                            @if (empty($trelloListArr))
                                List 동기화 버튼을 클릭하세요.
                            @endif
                            <button type="submit" class="btn btn-primary">List 동기화</button>
                        </div>
                    </div>
                </form>
            @endif

            @if ($trelloBoardId)
                <hr>
                <form action="{{ route('index.setting-update') }}" method="post">
                    @csrf
                    <input type="hidden" name="sync_member" value="1">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">
                            사용자 동기화
                        </label>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary">사용자 동기화</button>

                            @if (empty($members) || $members->isEmpty())
                                사용자 동기화 버튼을 클릭하세요.
                            @endif

                            <div>
                                @foreach ($members AS $m)
                                    {{ $m->full_name }} ({{ $m->username }})
                                    <br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </form>
            @endif


            @if ($requiredConfig)
                <hr>
                <form action="{{ route('index.setting-update') }}" method="post">
                    @csrf
                    <input type="hidden" name="sync_archive_cards" value="1">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">
                            아카이빙(closed) 된 카드 동기화
                        </label>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary">아카이빙(closed) 된 카드 동기화</button>
                            <br>
                            과거 카드를 동기화 하고 싶을 경우 클릭하세요
                        </div>
                    </div>
                </form>
            @endif


        </div>
    </div>
@endsection

@push('head-bot')
@endpush

@push('body-bot')
@endpush

