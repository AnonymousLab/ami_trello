<ul class="nav nav-tabs mb-2">
    <li class="nav-item">
        <a class="nav-link {{ Route::currentRouteName() === 'index' ? 'active' : '' }}"
           href="{{ route('index') }}">Gantt Chart</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::currentRouteName() === 'index.card-list' ? 'active' : '' }}"
           href="{{ route('index.card-list') }}">작업 리스트</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::currentRouteName() === 'index.setting' ? 'active' : '' }}"
           href="{{ route('index.setting') }}">Setting</a>
    </li>
</ul>
