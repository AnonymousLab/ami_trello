import 'dhtmlx-gantt';

gantt.plugins({
    tooltip: true
});
gantt.templates.tooltip_text = function(start,end,task){
    let desc = task.desc || null;
    let o = `<b>${task.text}</b>`;
    if (desc) {
        desc = desc.replace(/[\n\r]/g, '<br>');
        o += `<div>${desc}</div>`;
    }
    o += `<div><b>기간:</b> ${task.duration}일</div>`;

    return o;
};

gantt.config.date_format = "%Y-%m-%d %H:%i";

gantt.config.columns = [
    {name: "text", label: "Task name", tree: true, min_width: 200, width: 250, resize:true},
    {name: "start_date", label:"Start", align: "center", width: 80, resize:true},
    {name: "end_date", label:"End", align: "center", width: 80, template: function(task){
            return gantt.templates.date_grid(task.end_date, task);
        }, resize:true},
];

gantt.config.scale_height = 30*2;
gantt.config.min_column_width = 50;
gantt.config.scales = [
    { unit:"month", step:1, date:"%Y년 %m월" },
    { unit:"day", step:1, date:"%m-%d" }
];

// scales 가 두줄일 경우 작동을 안함
// gantt.templates.scale_cell_class = function (date) {
//     if (date.getDay() == 0 || date.getDay() == 6) {
//         return "weekend";
//     }
// };


// gantt.config.layout = {
//     css: "gantt_container",
//     cols: [
//         {
//             width:400,
//             min_width: 300,
//             rows:[
//                 {view: "grid", scrollX: "gridScroll", scrollable: true, scrollY: "scrollVer"},
//                 {view: "scrollbar", id: "gridScroll", group:"horizontal"}
//             ]
//         },
//         {resizer: true, width: 1},
//         {
//             rows:[
//                 {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
//                 {view: "scrollbar", id: "scrollHor", group:"horizontal"}
//             ]
//         },
//         {view: "scrollbar", id: "scrollVer"}
//     ]
// };

gantt.templates.grid_row_class = gantt.templates.task_row_class = function (start, end, task) {
    let css = ['custom_row'];
    css.push(`odd-${task.odd}`);
    if (task.is_header) {
        css.push('is-header');
    }
    return css.join(" ");
};

gantt.attachEvent("onParse", function() {
    gantt.eachTask(function(task) {
        // fill 'task.user' field with random data
        // task.user = Math.round(Math.random()*3);
        //
        if (gantt.hasChild(task.id)) {
            task.type = gantt.config.types.project
        }

        if (task.is_list) {
            task.type = gantt.config.types.project
        }
    });
});


gantt.templates.task_class = function (start, end, task) {
    let css = [];
    if (task.is_header) {
        css.push('d-none');
    }
    return css.join(" ");
};



gantt.attachEvent("onBeforeTaskDisplay", function(id, task){
    if (gantt_filter)
        if (task.owner !== gantt_filter)
            return false;

    return true;
});

let gantt_filter = gantt_filter || null;
function filterTasks(owner){
    gantt_filter = owner;
    gantt.scrollTo(0, 0);
    gantt.refreshData();
}


gantt.init("gantt_here");
gantt.parse(ganttArr);

// cantt filter
const ganttFilterSelectBox = document.querySelector('#js_filter_gantt_owner');
if (ganttFilterSelectBox) {
    ganttFilterSelectBox.addEventListener('change',function(){
        filterTasks(this.value);
    });
}

// list filter
const listFilterSelectBox = document.querySelectorAll('.js-filter-list');
if (listFilterSelectBox) {
    for (let i = 0; i < listFilterSelectBox.length; i++) {
        listFilterSelectBox[i].addEventListener('change',function(){
            filterList();
        });
    }
}

function filterList(page) {
    const xHttp = new XMLHttpRequest();
    const el = document.querySelector('.js-wrap-card-list');

    const sel1 = document.querySelector('#js_filter_list_member');
    const sel2 = document.querySelector('#js_filter_list_type');

    let url = '/api-card-list';
    const param = [];
    if (sel1 && sel1.value) {
        param.push(`member_id=${sel1.value}`);
    }
    if (sel2 && sel2.value) {
        param.push(`type=${sel2.value}`);
    }
    if (page) {
        param.push(`page=${page}`);
    }

    if (param.length) {
        url += '?' + param.join('&');
    }

    xHttp.onreadystatechange = function(){
        if(xHttp.readyState === 4 && xHttp.status === 200){
            el.innerHTML = xHttp.responseText;
            exec_body_scripts(el);
        }
    };

    xHttp.open('GET', url, true);
    xHttp.send();
}
// window.filterList = filterList;

/*
function exec_body_scripts(body_el) {
    function nodeName(elem, name) {
        return elem.nodeName && elem.nodeName.toUpperCase() ===
            name.toUpperCase();
    }

    function evalScript(elem) {
        var data = (elem.text || elem.textContent || elem.innerHTML || "" ),
            head = document.getElementsByTagName("head")[0] ||
                document.documentElement,
            script = document.createElement("script");

        script.type = "text/javascript";
        try {
            // doesn't work on ie...
            script.appendChild(document.createTextNode(data));
        } catch(e) {
            // IE has funky script nodes
            script.text = data;
        }

        head.insertBefore(script, head.firstChild);
        head.removeChild(script);
    }

    // main section of function
    var scripts = [],
        script,
        children_nodes = body_el.childNodes,
        child,
        i;

    for (i = 0; children_nodes[i]; i++) {
        child = children_nodes[i];
        if (nodeName(child, "script" ) &&
            (!child.type || child.type.toLowerCase() === "text/javascript")) {
            scripts.push(child);
        }
    }

    for (i = 0; scripts[i]; i++) {
        script = scripts[i];
        if (script.parentNode) {script.parentNode.removeChild(script);}
        evalScript(scripts[i]);
    }
}
*/
