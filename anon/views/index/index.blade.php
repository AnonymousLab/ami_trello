@extends('layouts.default')

@section('content')
    <div class="container-fluid pt-2">
        @include('index.index-tab')

        <div class="row">
            <div class="col-9">
                <form action="" class="form-inline mb-2">
                    Filter:
                    <select name="" id="js_filter_gantt_owner" class="custom-select">
                        <option value="">전체</option>
                        @foreach($members AS $mb)
                            <option value="{{ $mb->id }}">{{ $mb->full_name }}</option>
                        @endforeach
                    </select>
                </form>
            </div>

            <div class="col-3 text-right">
                <form action="{{ route('index.action') }}" method="post" onsubmit="return confirm('동기화? 오래걸림. 닫지 말것.')">
                    @csrf
                    <input type="hidden" name="type" value="trello_sync">
                    <button type="submit" class="btn btn-danger">수동 동기화</button>
                </form>
            </div>
        </div>

        <div class="wrap-gantt">
            <div id="gantt_here"></div>
            <script>ganttArr = @json($gantt);</script>
        </div>
    </div>

@endsection

@push('head-bot')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:regular,medium,thin,bold">
    <link rel="stylesheet" href="{{ mix2('index/index.css') }}">

    <script>
        var ganttArr = [];
    </script>
@endpush

@push('body-bot')
    <script src="{{ mix2('index/index.js') }}"></script>
@endpush
