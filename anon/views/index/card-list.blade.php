@extends('layouts.default')

@section('content')
    <div class="container pt-2">
        @include('index.index-tab')

        <div class="row">
            <div class="col-9">
                <form action="" class="form-inline mb-2">
                    Filter:
                    <select name="member_id" id="js_filter_list_member" class="js-filter-list custom-select">
                        <option value="">멤버 전체</option>
                        @foreach($members AS $mb)
                            <option value="{{ $mb->id }}"
                                {{ isset($get['member_id']) && $get['member_id'] === $mb->id ? 'selected' : '' }}
                            >{{ $mb->full_name }}</option>
                        @endforeach
                    </select>
                    <select name="type" id="js_filter_list_type" class="js-filter-list custom-select">
                        <option value="">Type 전체</option>
                        <option value="complete-card"
                            {{ isset($get['type']) && $get['type'] === 'complete-card' ? 'selected' : '' }}
                        >완료된 작업</option>
                        @foreach($listIds AS $li)
                            <option value="{{ $li->id }}"
                                {{ isset($get['type']) && $get['type'] === $li->id ? 'selected' : '' }}
                            >{{ $li->name }}</option>
                        @endforeach
                    </select>
                    <input type="submit" class="btn btn-info" value="검색">

                    @if (!empty($get['member_id']) || !empty($get['type']))
                        <a href="/list" class="btn btn-secondary">검색 초기화</a>
                    @endif
                </form>
            </div>

            <div class="col-3 text-right">
                <form action="{{ route('index.action') }}" method="post" onsubmit="return confirm('동기화? 오래걸림. 닫지 말것.')">
                    @csrf
                    <input type="hidden" name="type" value="trello_sync">
                    <button type="submit" class="btn btn-danger">수동 동기화</button>
                </form>
            </div>
        </div>



        <table class="table table-bordered table-sm table-striped">
            <colgroup>
                <col width="100px">
                <col width="100px">
                <col width="*">
                <col width="100px">
                <col width="100px">
                <col width="120px">
            </colgroup>
            <tr class="thead-dark text-center">
                <th>멤버</th>
                <th>Type</th>
                <th>Card</th>
                <th>시작일</th>
                <th>종료일</th>
                <th></th>
            </tr>

            @foreach ($cardList AS $card)
                <tr>
                    <td class="small text-center">
                        @foreach ($card->cardListWithMember AS $mb)
                            <div>{{ $memberArr[$mb->member_id]['full_name'] ?? '' }}</div>
                        @endforeach
                    </td>
                    <td class="small text-center">
                        @if ($card->id_list && isset($listIds[$card->id_list]))
                            {{ $listIds[$card->id_list]->name ?? '' }}
                        @endif
                    </td>
                    <td class="" style="word-break: break-all">
                        <a href="{{ $card->short_url }}" target="_blank" class="small">[보기]</a>
                        <b class="js-card-title-{{ $card->id }}">{{ $card->name }}</b>
                        <div class="small" >
                            {{ $card->desc  }}
                        </div>
                    </td>
                    <td class="small text-center">{{ substr($card->time_start, 0, 10) }}</td>
                    <td class="small text-center">
                        @if ($card->time_done)
                            {{ substr($card->time_done, 0, 10) }}
                        @elseif ($card->due)
                            {{ substr($card->due, 0, 10) }}
                            <br>
                            (예상일)
                        @endif
                    </td>
                    <td class="text-center">
                        @if ($card->id_list === $todoDoingDoneList['doing']['id'])
                            <button type="button" class="btn btn-xs btn-primary" onclick="cardComplete('{{ $card->id }}')">완료</button>
                        @endif
                        @if (empty($card->time_done))
                            <button type="button" class="btn btn-xs btn-danger" onclick="cardDelete('{{ $card->id }}')">삭제</button>
                        @endif

                        <button type="button" class="btn btn-xs btn-info" onclick="cardSync('{{ $card->id }}')">동기화</button>
                    </td>
                </tr>
            @endforeach
            @if ($cardList->isEmpty())
                <tr>
                    <td colspan="10">not found data</td>
                </tr>
            @endif
        </table>

        @if ($cardList->isNotEmpty())
            <div class="paging-center">
                {{ $cardList->render() }}
            </div>
        @endif
    </div>

@endsection

@push('head-bot')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:regular,medium,thin,bold">
    <link rel="stylesheet" href="{{ mix2('index/index.css') }}">
@endpush

@push('body-bot')
    <script>
        function cardUpdate(params) {
            const url = '/action';
            const xHttp = new XMLHttpRequest();
            xHttp.onreadystatechange = function(){
                if(xHttp.readyState === 4 && xHttp.status === 200){
                    const rtJson = JSON.parse(xHttp.responseText);
                    alert('완료');
                    location.reload();
                }
            };

            params._token = '{{ csrf_token() }}';
            params = typeof params == 'string' ? params : Object.keys(params).map(
                function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]) }
            ).join('&');

            xHttp.open('POST', url, true);
            xHttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xHttp.send(params);
        }

        function getTitle(cardId) {
            return document.querySelector('.js-card-title-' + cardId).innerText;
        }

        function cardComplete(cardId) {
            var cardTitle = getTitle(cardId);
            if (!confirm(cardTitle + "\n\n이 카드 완료?")) {
                return false;
            }
            cardUpdate({
                card_id: cardId,
                type: 'complete'
            })
        }

        function cardDelete(cardId) {
            var cardTitle = getTitle(cardId);
            if (!confirm(cardTitle + "\n\n이 카드 삭제?")) {
                return false;
            }

            cardUpdate({
                card_id: cardId,
                type: 'delete'
            })
        }

        function cardSync(cardId) {
            var cardTitle = getTitle(cardId);
            if (!confirm(cardTitle + "\n\n이 카드 동기화?")) {
                return false;
            }

            cardUpdate({
                card_id: cardId,
                type: 'sync'
            })
        }
    </script>
@endpush

