<!doctype html>
<html lang="en">
<head>
    <script src="{{ mix2('manifest.js') }}"></script>
    <script src="{{ mix2('vendor.js') }}"></script>

    @stack('head-top')
    @stack('head-top-js')

    <title>AMI trello</title>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @stack('head-mid')


    <link rel="stylesheet" href="{{ mix2('layouts/default.css') }}">


    @stack('head-bot-js')
    @stack('head-bot')
</head>
<body>
@stack('body-top')
@stack('body-top-js')

@yield('content')

@stack('body-bot-js')
@stack('body-bot')
</body>
</html>
